// ==UserScript==
// @name        Google Search By Date
// @namespace   Google Search By Date
// @include     http*://www.google.*/
// @include     http*://www.google.*/#hl=*
// @include     http*://www.google.*/search*
// @include     http*://www.google.*/webhp?hl=*
// @version     0.1.2
// @updateURL   https://bitbucket.org/jm78915/greasemonkey-scripts/raw/HEAD/Google-Search-By-Date.user.js
// @grant       none
// ==/UserScript==
const addNumber = 6;

window.addEventListener('load', () => {
  document.querySelector('#appbar').className = 'appbar hdtb-ab-o';
  document.querySelector('#hdtbMenus').className = 'hdtb-td-o';

  document.querySelectorAll('[id^=qdr_]:not([id="qdr_"])').forEach((dom) => {
    const m = dom.textContent.match(/(\D+)(\d+)(\D+)/);
    const prefix = m[1];
    const startIndex = +m[2];
    const postfix = m[3];

    const originA = dom.querySelector('.q.qs');

    const href = originA ? originA.href : location.pathname + location.search;

    const html = [];
    for (let i = startIndex; i < startIndex + addNumber; i += 1) {
      html.push(`
<a
class="q qs"
href="${href.replace(/(qdr:\w*)/, (match, p1, offset, string) => (p1 + i))}"
style="display: inline-block; padding: 0px 7px; width: 16px;"
>${i}</a>
        `);
    }
    dom.classList.remove('hdtbSel');
    dom.style.cssText = 'padding: 6px 44px 6px 30px;';
    dom.innerHTML = prefix + html.join('') + postfix;
  });
});
